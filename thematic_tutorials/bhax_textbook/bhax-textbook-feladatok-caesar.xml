<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Caesar!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    
    <section>
        <title><type>double **</type> háromszögmátrix</title>
        <para>Írj egy olyan <function>malloc</function> és <function>free</function> párost használó C programot, amely helyet foglal egy alsó háromszögmátrixnak a szabad tárban! </para>
        <para>
            Megoldás videó: <link xlink:href="https://www.youtube.com/watch?v=1MRTuKwRsB0">https://www.youtube.com/watch?v=1MRTuKwRsB0</link> 
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/nbatfai/bhax/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Caesar/tm.c">https://gitlab.com/nbatfai/bhax/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Caesar/tm.c</link>      
        </para>
        <para>
            <link xlink:href="https://gitlab.com/kincsa/bhax/blob/master/codes/caesar_codes/haromszogmatrix.c">https://gitlab.com/kincsa/bhax/blob/master/codes/caesar_codes/haromszogmatrix.c</link>
        </para>
 
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>

      <para>A háromszögmátrix csakis négyzetes mátrix lehet. Négyzetes mátrix az a mátrix, amelyben a         sorok száma megegyezik az oszlopok számával. A háromszögmátrix tehát olyan négyzetes mátrix, amelynek főátlója felett/alatt CSAKIS 0 található. Attól függően, hogy a 0-k a főátló felett/alatt helyezkednek el beszélhetünk felső/alsó háromszögmátrixról. A mi példánkban egy alsó háromszögmátrix fog elkészülni a következő program által:</para>

      <para>A kód elemzése:</para>
      
    <programlisting language="C"><![CDATA[
    #include <stdio.h>
    #include <stdlib.h>

    int main ()
    {
        int nr = 5;
        double **tm;
        printf("%p\n", &tm);
    ]]></programlisting>

    <para>Include-oljuk a szükséges függvénykönyvtárat, a program a main-nel kezdődik. Itt deklaráljuk az <varname>nr</varname> változót, ez lesz a mátrix sorainak száma. Mivel négyzetes mátrixról van szó, oszlopainak száma is ennyi lesz. Ezen kívül létrehozásra kerül a <varname>tm</varname>, double mutatóra mutató mutató, ami lefoglal a memóriából 8 bájtot. A <function>printf</function> függvénnyel kiiratjuk az előbb tárgyalt <varname>tm</varname> pointer memóribeli címét. </para>

    <programlisting language="C"><![CDATA[   
    if ((tm = (double **) malloc (nr * sizeof (double *))) == NULL)
    {
        return -1;
    }
    printf("%p\n", tm);
    ]]></programlisting>
    <para>
    Egy feltételvizsgálat jön velünk szembe, mely tartalmazza a lespoilerezett <function>malloc</function> függvényt. A <function>malloc</function> a zárójelben szereplő értéknek megfelelő bájtmennyiséget foglal le a memóriából, visszatérési értéke egy <varname>void</varname> típusú pointer. A lefoglalt bájtmennyiség ebben az esetben <function>nr</function>-szer <function>double *</function> lesz, vagyis 5*8, 40 bájt. Típuskényszerítjük a <function>malloc</function>-ot, hiszen nekünk nem void típusú pointerre, sokkal inkább egy double**-ra lenne szükségünk. Ha a <function>tm</function> NULL értékre mutat, vagyis ha nem sikerült a <function>malloc</function> memóriafoglalása, -1-gyel tér vissza a program jelezve azt, hogy hiba történt a végrehajtás során. A <function>malloc</function>-kal lefoglalt memóriaterület címét kapja meg a <function>tm</function>, ez kerül kiiratásra. 
    </para>
 
    <programlisting language="C"><![CDATA[
     for (int i = 0; i < nr; ++i)
    {
        if ((tm[i] = (double *) malloc ((i + 1) * sizeof (double))) == NULL)
        {
            return -1;
        }
    }
    printf("%p\n", tm[0]);    

    ]]></programlisting>

    <para>
      Indítunk egy for ciklust mely törzsében ugyanazt vizsgáljuk mint az előző kódrészletben. A <function>malloc</function> most is típuskényszerítésre kerül, <function>double *</function>-gal. Az előzőtől különböző módon itt a mutatót úgy használjuk, mintha tömb lenne. (A tömbök és mutatók felcserélésére egyébként gyakran láthatunk példát más programokban is). Kiiratásra kerül a <function>tm</function> által tárolt memóriacím. A <function>malloc</function>  most egyébként <function>i+1</function>-szer <function>double</function>-nyi memóriát foglal le, ez biztosítja azt hogy sorról-sorra több elemmel legyen feltöltve a háromszögmátrix, kialakítva ezzel jellegzetes alakját.
    </para>

    <programlisting language="C"><![CDATA[
    for (int i = 0; i < nr; ++i)
        for (int j = 0; j < i + 1; ++j)
            tm[i][j] = i * (i + 1) / 2 + j;

    for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf ("%f, ", tm[i][j]);
        printf ("\n");
    }
    ]]></programlisting>

    <para>
      Két egymásba ágyazott for ciklussal a <varname>tm</varname> (látszat)tömb elemeit feltöltjük fent olvasható, <function>tm[i][j] = i * (i + 1) / 2 + j</function> képlet segítségével. A második for ciklus az elemek kiiratásáért felel.
    </para>

<programlisting language="C"><![CDATA[
     tm[3][0] = 42.0;
    (*(tm + 3))[1] = 43.0;
    *(tm[3] + 2) = 44.0;
    *(*(tm + 3) + 3) = 45.0;

    for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf ("%f, ", tm[i][j]);
        printf ("\n");
    }
]]></programlisting>

  <para>
    Itt a mutatókkal ügyeskedtünk egy kicsit. Pointerek segítségével töltöttük fel a háromszögmátrixunk 4. sorát a 42.0 értéktől kezdődően. Látható, hogy az értékadás mind a 4 esetben különbözőféleképpen megy végbe. Ez a mutatók varázsa. Egy for cikluson belül most sem marad el a kiiratás, az értékeket természetesen megjelenítjük az outputon. Ez volt a háromszögmátrix 4. sora.
  </para>

  <para>A program ezen részének megértéséhez azonban elengedhetetlen a mutatók/pointerek alaposabb szemrevételezése. Erre tökéletes a Bátfai Norbert Tanár Úr által elkészített szemléletes ábra melyet a példámban megjelenő memóriacímekkel módosítottam.</para>

      <mediaobject>
                  <imageobject>
                      <imagedata fileref="images/doublecscs.png" scale="200"/>
                  </imageobject>
      </mediaobject>

      <para>A kép forrásának linkje: <link xlink:href="https://gitlab.com/nbatfai/bhax/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/img/doublecscs.png">eredeti</link>
      </para>

      <para>A kép forrásának linkje: <link xlink:href="https://gitlab.com/kincsa/bhax/tree/master/thematic_tutorials/bhax_textbook/images/doublecscs.png">saját</link>
      </para>

  <programlisting language="C"><![CDATA[
    for (int i = 0; i < nr; ++i)
      free (tm[i]);

    free (tm);
]]></programlisting>

    <para>
      A <function>free</function> függvénnyel az előzőleg, <function>malloc</function> függvény által lefoglalt memóriamennyiséget szabadítjuk fel, hiszen nem használjuk tovább.
    </para>

    <para>A programunk ezzel véget is ért, fordítás és futtatás után kész a háromszögmátrixunk! </para>

      <mediaobject>
            <imageobject>
                <imagedata fileref="images/matrix.png" scale="50"/>
            </imageobject>
        </mediaobject>

    </section> 

    <section>
        <title>C EXOR titkosító</title>
        <para>
            Írj egy EXOR titkosítót C-ben!
        </para>
        <para>
            Megoldás videó:
        </para>
        <para>Megoldás forrása:</para>
        <para>
            <link xlink:href="https://sourceforge.net/p/udprog/code/ci/master/tree/source/labor/exor/e.c               ">https://sourceforge.net/p/udprog/code/ci/master/tree/source/labor/exor/e.c</link>      
        </para>
        <para>
            <link xlink:href="https://gitlab.com/kincsa/bhax/blob/master/codes/caesar_codes/e.c">https://gitlab.com/kincsa/bhax/blob/master/codes/caesar_codes/e.c</link>
        </para>
        <para>Tutor: Szegedi Csaba -<link xlink:href="https://gitlab.com/dev.csaba.szegedi">https://gitlab.com/dev.csaba.szegedi</link> </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
        
        <para>Ahogy arra a feladat megnevezéséből is következtetni lehetett, a programunk célja nem más, mint hogy egy számára átadott szöveget titkosítson, vagyis hogy ellehetetlenítse a szöveg olvasását. Erre nem mást, mint az XOR (EXOR) vagyis kizáró vagy logikai műveletet alkalmazza, amivel már találkoztunk a Helló, Turing! fejezet 2.3-as sorszámú feladatában. </para>
         <para>
            A programkód elemzése:
         </para>
    <programlisting language="C"><![CDATA[
    #include <stdio.h>
    #include <unistd.h>
    #include <string.h>

    #define MAX_KULCS 100
    #define BUFFER_MERET 256
    ]]></programlisting>

    <para>
        Include-olásra kerülnek függvénykönyvtáraink és bevezetjük nevesített konstainsainkat: <varname>MAX_KULCS</varname>, melynek értéke 100 (a töréshez használt kulcs), illetve <varname>BUFFER_MERET</varname>, mely értéke 256 (amiben az eredmény lesz eltárolva) lett. Későbbiekben a programunkban az előző konstansok (ahogy a nevük is sugallja) végig ezekkel az értékkel fognak rendelkezni.
    </para>
    <programlisting language="C"><![CDATA[
    int main (int argc, char **argv)
    {
      char kulcs[MAX_KULCS];
      char buffer[BUFFER_MERET];

      int kulcs_index = 0;
      int olvasott_bajtok = 0;

      int kulcs_meret = strlen (argv[1]);
      strncpy (kulcs, argv[1], MAX_KULCS);
    ]]></programlisting>

    <para>
        Kezdődik a <function>main</function> függvényünk. Ebben a pár sorban kerül létrehozásra a két, char típusú tömbünk melyek 100 és 256 méretűek lesznek. (Természetesen az előbb tárgyalt nevesített konstansok miatt.) Az egyikben a kulcs, míg másikban a már beolvasott karakterek lesznek eltárolva. Továbbá a <varname>kulcs_index</varname> értelemszerűen a kulcs indexét, míg az <varname>olvasott_bajtok</varname>ban tárolódnak a már beolvasott bájtok. A <varname>kulcs_meret</varname> változó értékét a futtatáskor megadott parancssori argumentum <function>strlen</function>-nel visszaadott hossza adja. A <varname>kulcs</varname> tömbbe másolódik az előbb parancssori argumentumként megadott, titkosítandó szöveg minden egyes karaktere <varname>strncpy</varname> használatával, ugyanis <varname>MAX_KULCS</varname>-ig megy a másolás. Ebből következik, hogy minden karakter másolásra kerül.
    </para>
    
    <programlisting language="C"><![CDATA[
    while ((olvasott_bajtok = read (0, (void *) buffer, BUFFER_MERET)))
    {
        for (int i = 0; i < olvasott_bajtok; ++i)
      {

        buffer[i] = buffer[i] ^ kulcs[kulcs_index];
        kulcs_index = (kulcs_index + 1) % kulcs_meret;

      }
        write (1, buffer, olvasott_bajtok);
    }
    ]]></programlisting> 

    <para>
        Ez a pár sor programunk egyik legfontosabb építőköve. Egy while ciklus az, ami közrefogja ezt a szakaszt mely ciklus addig fut, amíg van beolvasandó bájt. Ezt követően egy belső for ciklussal találkozunk, amely törzsében:
        A <varname>buffer</varname> tömb i-edik elemének értéke az i-edik elem és a <varname>kulcs</varname> tömb kulcs_index-edik elemének vett EXOR műveleteredménye lesz. A <varname>kulcs_index</varname> értékét a fent látható módon növeljük.
        Végül kiiratásra kerül a <varname>buffer</varname> összes eleme, vagyis maga a titkosított szövegünk:
    </para>

    <mediaobject>
            <imageobject>
                <imagedata fileref="images/titkos_c.png" scale="60"/>
            </imageobject>
        </mediaobject>

        <para>
            Érdekesség, hogy ha újra futtatjuk a programot immáron a titkosított szöveget megadva inputként, az EXOR-ozás természetesen újra végbemegy és megkapjuk eredeti, titkosítatlan szövegünket:
        </para>

        <mediaobject>
            <imageobject>
                <imagedata fileref="images/titkos_c2.png" scale="60"/>
            </imageobject>
        </mediaobject>


    </section>
    <section>
        <title>Java EXOR titkosító</title>
        <para>
            Írj egy EXOR titkosítót Java-ban!
        </para>
        <para>
            Megoldás videó:
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/ch01.html">https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/ch01.html</link> 1.12. példája - Titkosítás kizáró vaggyal és
        </para>
        <para><link xlink:href="https://gitlab.com/kincsa/bhax/blob/master/codes/caesar_codes/ExorTitkos%C3%ADt%C3%B3.java">https://gitlab.com/kincsa/bhax/blob/master/codes/caesar_codes/ExorTitkos%C3%ADt%C3%B3.java</link></para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
    <para>Az elv ugyanaz, csupán a nyelv más. Kezdjünk bele a Java titkosítónk elemzésébe! </para>
    <programlisting language="Java"><![CDATA[
    public class ExorTitkosító {
    ]]></programlisting>
    <para>Egy nyilvános(an hozzáférhető) osztályban történik az algoritmusunk megírása. Ez az első sor, a C-től eltérően itt nincs szükség függvénykönyvtárak include-olására. </para>
<programlisting language="Java"><![CDATA[
    public ExorTitkosító(String kulcsSzöveg,
    java.io.InputStream bejövőCsatorna,
    java.io.OutputStream kimenőCsatorna)
]]></programlisting>
<para>
    Létrehozásra kerül az <varname>ExortTitkosító</varname> objektumunk, ami 3 paramétert kap majd. Magát a kulcsként használt szöveget, a be-illetve kimenő csatornát vagyis az inputot és outputot.
</para>
 
<programlisting language="Java"><![CDATA[
    throws java.io.IOException {
            
    byte [] kulcs = kulcsSzöveg.getBytes();
    byte [] buffer = new byte[256];
    int kulcsIndex = 0;
    int olvasottBájtok = 0;
]]></programlisting>

<para>Az első sor kivételkezelés szempontjából igen fontos. Ha valamilyen input vagy output hibát talál a program, jelzi majd felénk. Definiáljuk a <varname>kulcs</varname> és definiáljuk <varname>buffer</varname>, byte típusú tömbjeinket. Az elsőbe természetesen a kulcsszöveg kerül majd, míg a másodiknak egyelőre csak a méretét adtuk meg. Két integer változó is létrehozásra került a kulcs indexének és a már beolvasott bájtok számának számontartására.</para>

<programlisting language="Java"><![CDATA[
    while((olvasottBájtok =
                bejövőCsatorna.read(buffer)) != -1) {
            
            for(int i=0; i<olvasottBájtok; ++i) {
                
                buffer[i] = (byte)(buffer[i] ^ kulcs[kulcsIndex]);
                kulcsIndex = (kulcsIndex+1) % kulcs.length;       
            }
            
            kimenőCsatorna.write(buffer, 0, olvasottBájtok);   
        }
]]></programlisting>

<para>
    Ha jobban megnézzük, lényegében szinte ugyanaz történik, mint a C-s megoldásunkban! A while ciklus addig fut, amíg a bufferből van mit kiolvasni a <function>read</function> függvénnyel. Tehát amíg van beolvasandó bájtunk.
        A <varname>buffer</varname> tömb i-edik elemének értéke az i-edik elem és a <varname>kulcs</varname> tömb kulcs_index-edik elemének vett EXOR műveleteredménye lesz, ez a művelet eredményezi majd a számunkra olvashatatlan szöveget. Itt típuskényszerítést alkalmaztunk, amikor az EXOR művelet elé tettük (byte) kulcsszót. Ezzel a művelet eredménye byte típusú lesz. A <varname>kulcs_index</varname> értékét a fent látható módon növeljük.
        Végül kiiratásra kerülnek a <varname>buffer</varname> elemei 0-tól <varname>olvasottBájtok</varname>-ig, a <function>write</function> függvénnyel az outputra. (<varname>kimenőCsatorna</varname>)
</para>

 
<programlisting language="Java"><![CDATA[
    public static void main(String[] args) {
        
        try {
            
            new ExorTitkosító(args[0], System.in, System.out);
            
        } catch(java.io.IOException e) {
            
            e.printStackTrace();
        }   
    }
]]></programlisting>

<para>
    Eljutottunk a <function>main</function>-be, ahol egy try-catch szerkezet fogad minket. A try-on belül megtörténik a példányosítás. Létrehozunk egy új ExorTitkosítót a <emphasis role="strong">new</emphasis> kulcsszó segítségével amely a parancssori argumentumként kapott szöveget fogja átalakítani. A catch-ben meghívásra kerül a <function>printStackTrace</function> function, ami egy igen erős és hasznos eszköz kivételkezelés szempontjából, ugyanis megmondja, mi a probléma forrása és hogy az hol található.
</para>
<para>Nem maradt más dolgunk, minthogy fordítsuk és futtassuk Java programunkat! </para>

<screen><![CDATA[
akos@akos-VirtualBox:~/Asztal/bhax/codes$ javac ExorTitkosító.java
akos@akos-VirtualBox:~/Asztal/bhax/codes$ java ExorTitkosító valami > titkos.txt
Lássuk, hogyan működik a titkosító!
]]></screen>

<para> A titkos.txt fájlba szeretnénk a "Lássuk, hogyan működik a titkosító!" szövegünket titkosítani. Lássuk, sikerült-e!</para>

<mediaobject>
            <imageobject>
                <imagedata fileref="images/java1.png" scale="70"/>
            </imageobject>
        </mediaobject>


<para>A more paranccsal kiiratjuk a fájl tartalmát és tényleg látható, hogy egy számunkra olvashatatlan szöveget kaptunk. Tehát az algoritmus helyes! Próbáljuk meg visszaalakítani! </para>

<screen><![CDATA[
akos@akos-VirtualBox:~/Asztal/bhax/codes$ java ExorTitkosító valami < titkos.txt
Lássuk, hogyan működik a titkosító!
]]></screen>

<para>Tehát ha újra titkosítjuk a már eleve titkosított szövegünket, megkapjuk az eredeti szöveget! Így működik a Java titkosító és egyben törő. </para>

        
    </section>        
    <section>
        <title>C EXOR törő</title>
        <para>
            Írj egy olyan C programot, amely megtöri az első feladatban előállított titkos szövegeket!
        </para>
        <para>
            Megoldás videó:
        </para>
        <para>
            Megoldás forrása:   <link xlink:href="https://sourceforge.net/p/udprog/code/ci/master/tree/source/labor/exor/t.c">https://sourceforge.net/p/udprog/code/ci/master/tree/source/labor/exor/t.c</link>             
        </para>
        <para>
            <link xlink:href="https://gitlab.com/kincsa/bhax/blob/master/codes/caesar_codes/t.c">https://gitlab.com/kincsa/bhax/blob/master/codes/caesar_codes/t.c</link>
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
        <para>
            Ha már az előzőekben megírtuk C-ben a titkosítót, illene egy törőt is, nem igaz? Mert igaz, hogy a titkosítóval képesek vagyunk törésre is, azonban csak abban az esetben ha ismerjük a kulcsot. A következőkben nézett programban csupán a kulcs méretet szükséges tudnunk a kulcs előállításához. A következőkben a törőt fogjuk megnézni.
        </para>
        <programlisting language="C"><![CDATA[
        #define MAX_TITKOS 4096
        #define OLVASAS_BUFFER 256
        #define KULCS_MERET 8
        #define _GNU_SOURCE

        #include <stdio.h>
        #include <unistd.h>
        #include <string.h>
        ]]></programlisting>
        <para>
            A titkosító programhoz hasonló résszel találkozunk. Az include-olt függvénykönyvtárak ugyanazok, mint előző alkalommal, ebben nincs változás. Ugyanúgy nevesített konstansokat használunk majd ebben a programban is, az első 3 sorban vezetjük be őket. A program során az itt megadott névvel hivatkozunk rájuk, értékük végig adott lesz. Továbbá definiálásra kerül a <varname>_GNU_SOURCE</varname> is, ami segítségével számos új function-höz férhetünk majd hozzá, ilyen lesz például az <function>strcasestr</function> is.
        </para>
        <programlisting language="C"><![CDATA[
        double atlagos_szohossz (const char *titkos, int titkos_meret)
        {
          int sz = 0;
          for (int i = 0; i < titkos_meret; ++i)
            if (titkos[i] == ' ')
              ++sz;

          return (double) titkos_meret / sz;
        }
         ]]></programlisting>

       <para>Itt kezdődik a függvények definiálása. Ez a függvény a szavak átlagos hosszát hivatott visszaadni egy double érték formájában és két paramétert kap. A szóközöket számolja, ha talál, az <varname>sz</varname> változóz növeli. Visszatérési értéke a méretet és a szószámot tartalmazó változókból megkapott átlag, melyet típuskényszerítunk double-re.
       </para>

       <programlisting language="C"><![CDATA[
        int tiszta_lehet (const char *titkos, int titkos_meret)
        {
          double szohossz = atlagos_szohossz (titkos, titkos_meret);

          return szohossz > 6.0 && szohossz < 9.0
            && strcasestr (titkos, "hogy") && strcasestr (titkos, "nem")
            && strcasestr (titkos, "az") && strcasestr (titkos, "ha");
        }
         ]]></programlisting> 
         <para>
            A <function>tiszta_lehet</function> függvény célja az, hogy kiszűrjük a már eleve tiszta szöveget, szövegrészeket. A függvényben létrehozásra kerül egy új, double típusú változó, aminek értéke az előbb létrehozott <function>atlagos_szohossz</function> függvényből származik majd. Vagyis tárolja az input fájl átlag szóhosszát. Megvizsgálja, hogy a fájl tartalmazza-e a leggyakoribb magyar szavakat, mert ha igen, valószínűleg már tiszta szövegről van szó. Ezt a <function>strcasestr</function>-rel tesszük meg. Illetve az átlag szóhosszt is vizsgálja, ami a magyar nyelvben hozzávetőlegesen 6 és 9 közötti. Ezt is felhasználja ahhoz, hogy el tudja dönteni, tiszta vagy épp még titkos szövegről van szó. A visszatérési érték egész lesz.
         </para>
         <programlisting language="C"><![CDATA[
        void exor (const char kulcs[], int kulcs_meret, char titkos[], int titkos_meret)
        {
          int kulcs_index = 0;
          for (int i = 0; i < titkos_meret; ++i)
            {
              titkos[i] = titkos[i] ^ kulcs[kulcs_index];
              kulcs_index = (kulcs_index + 1) % kulcs_meret;
            }
        }
         ]]></programlisting> 
         <para>A függvény a paraméterként megkapott tömbök (<varname>kulcs</varname>, illetve <varname>titkos</varname>) bitjein rendre elvégzi az EXOR műveletet, ahogyan azt a neve is sugallja számunkra. Itt is a szokásokhoz híven a ^ operátor segítségével történik a művelet. Mivel a függvény void típusú, így nincs visszatérési értéke, csak végrehajt.</para>
          <programlisting language="C"><![CDATA[
            int
            exor_tores (const char kulcs[], int kulcs_meret, char titkos[],
                    int titkos_meret)
            {

              exor (kulcs, kulcs_meret, titkos, titkos_meret);

              return tiszta_lehet (titkos, titkos_meret);

            }
         ]]></programlisting>
         <para>
            Ez a függvény lényegében egyesíti az eddig külön-külön már az előbb tárgyalt függvényeinket. Megtörténik az <function>exor</function> fv. meghívása, valamint a <function>tiszta_lehet</function>-é is, amely által produkált eredmény a függvény visszatérési értéke lesz. (0 vagy 1)
         </para>
          <programlisting language="C"><![CDATA[
            int main (void)
             {
                char kulcs[KULCS_MERET];
                char titkos[MAX_TITKOS];
                char *p = titkos;
                int olvasott_bajtok;

         ]]></programlisting>
         <para>
            Itt kezdődik a main-ünk. Inicializálásra kerülnek a <varname>kulcs</varname> és <varname>titkos</varname> tömbök a megfelelő értékekkel. Létrehozásra kerül egy char típusú <varname>p</varname> pointer, ami <varname>titkos</varname>ra mutat. A beolvasott bájtok számontartására is definiálásra kerül egy változó, <varname>olvasott_bajtok</varname> néven.
         </para>
         <programlisting language="C"><![CDATA[
         // a titkos fajl berantasa
          while ((olvasott_bajtok =
              read (0, (void *) p,
                (p - titkos + OLVASAS_BUFFER <
                 MAX_TITKOS) ? OLVASAS_BUFFER : titkos + MAX_TITKOS - p)))
                    p += olvasott_bajtok;

          // maradek hely nullazasa a titkos bufferben  
          for (int i = 0; i < MAX_TITKOS - (p - titkos); ++i)
            titkos[p - titkos + i] = '\0';
        ]]></programlisting>
        <para>
            Mi történik itt? Ahogy azt megjegyzésben olvasni lehet, maga a titkos fájl berántása. Egy while ciklusban történik a művelet aminek ciklusfejében lévő feltétel teljesülése esetén <varname>p</varname> értékét növelem az olvasott_bajtokéval. Addig olvasom a bufferbe a bájtokat, míg van mit. Továbbá a bufferben fennmaradó helyet nullázzuk, azokkal nem kívánunk a feladat során foglalkozni.
        </para>
        <programlisting language="C"><![CDATA[
        //az osszes kulcs eloallitasa
          for (int ii = '0'; ii <= '9'; ++ii)
            for (int ji = '0'; ji <= '9'; ++ji)
              for (int ki = '0'; ki <= '9'; ++ki)
                for (int li = '0'; li <= '9'; ++li)
                  for (int mi = '0'; mi <= '9'; ++mi)
                    for (int ni = '0'; ni <= '9'; ++ni)
                      for (int oi = '0'; oi <= '9'; ++oi)
                        for (int pi = '0'; pi <= '9'; ++pi)
                          {
                            kulcs[0] = ii;
                            kulcs[1] = ji;
                            kulcs[2] = ki;
                            kulcs[3] = li;
                            kulcs[4] = mi;
                            kulcs[5] = ni;
                            kulcs[6] = oi;
                            kulcs[7] = pi;

                            if (exor_tores (kulcs, KULCS_MERET, titkos, p - titkos))
                              printf
                            ("Kulcs: [%c%c%c%c%c%c%c%c]\nTiszta szoveg: [%s]\n",
                             ii, ji, ki, li, mi, ni, oi, pi, titkos);

                            // ujra EXOR-ozunk, igy nem kell egy masodik buffer  
                            exor (kulcs, KULCS_MERET, titkos, p - titkos);
                          }

                  return 0;
                }
            ]]></programlisting>
            <para>
                A program utolsó szekciójához értünk. 8 egymásba ágyazott for ciklussal előállítjuk az összes lehetséges kulcsot, amelyet aztán a <varname>kulcs</varname> tömbbe tárolunk el. Jelen feladatban 8 bitesként kezeljük a kulcshosszt, ezért indokolt a 8 for ciklus, feltételezve azt, hogy 8 tagú a kulcsunk. A feladat során most épp annyi. Ha megvan a kulcs, kiiratásra kerül a tiszta, már titkosítatlan szöveg.
            </para>
    </section>        
    <section>
        <title>Neurális OR, AND és EXOR kapu</title>
        <para>
            R
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/Koyw6IH5ScQ">https://youtu.be/Koyw6IH5ScQ</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/nbatfai/bhax/tree/master/attention_raising/NN_R">https://gitlab.com/nbatfai/bhax/tree/master/attention_raising/NN_R</link>               
        </para>
        <para>
            <link xlink:href="https://gitlab.com/kincsa/bhax/blob/master/codes/caesar_codes/neuralis.r">https://gitlab.com/kincsa/bhax/blob/master/codes/caesar_codes/neuralis.r</link>
        </para>
        <para>Tutoriált: Szegedi Csaba -<link xlink:href="https://gitlab.com/dev.csaba.szegedi">https://gitlab.com/dev.csaba.szegedi</link> </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>

        <para>
        Elöljáróban: mik azok a neurális hálózatok, miért is jók nekünk? Manapság a fogalmat kétféleképpen értelmezhetjük. Léteznek biológiai- és mesterséges neurális hálózatok.
        Minket utóbbi fog érdekelni. A mesterséges neurális hálózatok működése a biológiai hálózatok pár tulajdonságán alapszik. Az egyik legfontosabb jellemzője, hogy képes egy átadott minta alapján tanulni vagyis megvalósul a gépi tanulás. A következő feladatban egy R nyelvben írt programot fogunk kielemezni, hogyan is képes egyes logikai műveletek megtanulására.
        </para>

    <programlisting language="R"><![CDATA[
    library(neuralnet)
    ]]></programlisting>

      <para>
          A <varname>neuralnet</varname> könyvtárra van szükségünk a feladat megoldásához.        
      </para>

    <programlisting language="R"><![CDATA[
    a1    <- c(0,1,0,1)
    a2    <- c(0,0,1,1)
    OR    <- c(0,1,1,1)

    or.data <- data.frame(a1, a2, OR)
    ]]></programlisting>

      <para>Létrehozzuk az <varname>a1</varname>, <varname>a1</varname> változóinkat a képen látható bájtokat tartalmazva és az <varname>OR</varname> változót, amely az azonos indexű biteken végzett VAGY művelet eredményét adja vissza. A <function>data.frame</function> függvénnyel egy adatkeretet létrehozunk ezekből a változókból majd ez bekerül a program data (adat) részébe, ezekből az adatokból fogja megtanulni a háló a művelet megoldását.</para>

      <programlisting language="R"><![CDATA[
      nn.or <- neuralnet(OR~a1+a2, or.data, hidden=0, linear.output=FALSE, stepmax = 1e+07, threshold = 0.000001)

      plot(nn.or)

      compute(nn.or, or.data[,1:2])
      ]]></programlisting>

      <para>Az <varname>nn.or</varname> kerül a <function>neuralnet</function> függvény által visszaadott érték, mely függvény számos paraméterrel rendelkezik.</para>
      <para>Ezt követően a <function>plot</function> függvénnyel kirajzoltatjuk az eredményt. </para>
      <para>A <function>compute</function> függvénnyel pedig kiszámoltatjuk az eredményt.</para>

      <screen><![CDATA[
      $neurons[[1]]
              a1 a2
      [1,] 1  0  0
      [2,] 1  1  0
      [3,] 1  0  1
      [4,] 1  1  1


      $net.result
                  [,1]
      [1,] 0.001433089
      [2,] 0.999362223
      [3,] 0.999118762
      [4,] 0.999999999

      ]]></screen>

       <mediaobject>
                  <imageobject>
                      <imagedata fileref="images/neuralis1.png" scale="50"/>
                  </imageobject>
      </mediaobject>

      <para>Ebből is látszik, hogy helyes eredményt kaptunk! </para>


      <programlisting language="R"><![CDATA[
        a1    <- c(0,1,0,1)
        a2    <- c(0,0,1,1)
        OR    <- c(0,1,1,1)
        AND   <- c(0,0,0,1)

        orand.data <- data.frame(a1, a2, OR, AND)

        nn.orand <- neuralnet(OR+AND~a1+a2, orand.data, hidden=0, linear.output=FALSE, stepmax = 1e+07, threshold = 0.000001)

        plot(nn.orand)

        compute(nn.orand, orand.data[,1:2])

      ]]></programlisting>

      <para>Ugyanezt a folyamatot játszuk le akkor, amikor a logikai ÉS műveletet tanítjuk meg a hálózatnak. Lent láthatjuk, hogy az eredmény itt is jó:</para>
      <screen><![CDATA[
      $neurons[[1]]
              a1 a2
      [1,] 1  0  0
      [2,] 1  1  0
      [3,] 1  0  1
      [4,] 1  1  1


      $net.result
                   [,1]         [,2]
      [1,] 5.697714e-05 2.627334e-09
      [2,] 9.999619e-01 1.316939e-03
      [3,] 9.999816e-01 1.253087e-03
      [4,] 1.000000e+00 9.984145e-01
      ]]></screen>

      <mediaobject>
                  <imageobject>
                      <imagedata fileref="images/neuralis2.png" scale="50"/>
                  </imageobject>
      </mediaobject>


      <programlisting language="R"><![CDATA[
        a1      <- c(0,1,0,1)
        a2      <- c(0,0,1,1)
        EXOR    <- c(0,1,1,0)

        exor.data <- data.frame(a1, a2, EXOR)

        nn.exor <- neuralnet(EXOR~a1+a2, exor.data, hidden=0, linear.output=FALSE, stepmax = 1e+07, threshold = 0.000001)

        plot(nn.exor)

        compute(nn.exor, exor.data[,1:2])

      ]]></programlisting>
      <para>Ha már 2 logikai műveletet sikeresen megtanítottunk neki, miért is ne tudná megtanulni a 3.-at is, nem igaz? Az EXOR (XOR/kizáró vagy) műveletét tanítjuk meg itt neki. </para>


      <screen><![CDATA[
      $neurons[[1]]
             a1 a2
      [1,] 1  0  0
      [2,] 1  1  0
      [3,] 1  0  1
      [4,] 1  1  1


      $net.result
                [,1]
      [1,] 0.5000021
      [2,] 0.5000013
      [3,] 0.5000003
      [4,] 0.4999995

      ]]></screen>

      <mediaobject>
                  <imageobject>
                      <imagedata fileref="images/neuralis_exor.png" scale="50"/>
                  </imageobject>
      </mediaobject>

<para>Látható, hogy mindenhol köztes értékeket kaptunk, tehát hiba csúszott a gépezetbe. Itt 50 %-os hibaarányról beszélünk.</para>

<para>Na de mi lehet a megoldás?</para>
  
  <programlisting language="R"><![CDATA[

        nn.exor <- neuralnet(EXOR~a1+a2, exor.data, hidden=c(6, 4, 6), linear.output=FALSE, stepmax = 1e+07, threshold = 0.000001)
      ]]></programlisting>

      <para>Ez, a fenti egy sor változik. Ha bevezetünk rejtett neuronokat, helyes végeredményt kapunk! </para>

    <screen><![CDATA[
          $net.result
                 [,1]
    [1,] 0.0003154876
    [2,] 0.9991795416
    [3,] 0.9995412678
    [4,] 0.0009422256
    ]]></screen>

    <mediaobject>
                  <imageobject>
                      <imagedata fileref="images/neuralis_exor2.png" scale="50"/>
                  </imageobject>
      </mediaobject>

      <para>
        Ezzel kijelenthetjük, hogy sikeres volt a gépi tanulás, képesek voltunk a tanításra.
    </para>

    </section>

        <section>
        <title>Hiba-visszaterjesztéses perceptron</title>
        <para>
            C++
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://www.youtube.com/watch?v=XpBnR31BRJY">https://www.youtube.com/watch?v=XpBnR31BRJY</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/kincsa/bhax/blob/master/codes/caesar_codes/main.cpp">https://gitlab.com/kincsa/bhax/blob/master/codes/caesar_codes/main.cpp</link>
        </para><para> <link xlink:href="https://gitlab.com/kincsa/bhax/blob/master/codes/caesar_codes/mlp.hpp">https://gitlab.com/kincsa/bhax/blob/master/codes/caesar_codes/mlp.hpp</link>             
        </para>
        <para>Tutoriált: Heinrich László -<link xlink:href="https://gitlab.com/heinrichlaszlo">https://gitlab.com/heinrichlaszlo</link> </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
        <para>
          Érdemes lenne először a fogalmat megmagyarázni. A Neurális OR, AND és EXOR kapu feladatnál már találkozhattunk a neuron és a gépi tanulás fogalmával. A perceptron nem más mint ezen neuron mesterséges intelligenciában használt változata. Szintén tanulásra képes, a bemenő 0-k és 1-esek sorozatából mintákat tanul meg és súlyozott összegzést végez.
        </para>
        <para>A következő feladat során egy ilyen perceptront fogunk elkészíteni, aminek alapvetően a feladata az, hogy a <function>mandelbrot.cpp</function> programunk által létrehozott Mandelbrot-halmazt ábrázoló PNG kép egyik színkódját vegye és az a színkód legyen a többrétegű neurális háló inputja. Lássuk a programot!</para>

        <programlisting language="C"><![CDATA[   
          #include <iostream>
          #include "mlp.hpp"
          #include <png++/png.hpp>
    ]]></programlisting>

        <para>Include-oljuk az iostream, az <function>mlp</function> és a <function>png++/png</function> könyvtárakat. Utóbbi kettőt azért, mert többrétegű perceptront akarunk majd létrehozni (Multi Layer Perceptron), így muszáj ezt a könyvtárat include-olnunk a programunkba. Utolsó könyvtárunk ahogy a neve is sugallja, a PNG képállományokkal való munkát teszi lehetővé. </para>

        <programlisting language="C"><![CDATA[   
          using namespace std;

          int main(int argc, char **argv)
          {
            png::image <png::rgb_pixel> png_image(argv[1]);

            int size = png_image.get_width()*png_image.get_height();

            Perceptron *p = new Perceptron(3, size, 256, 1);
    ]]></programlisting>

      <para>Kezdődik a main függvényünk. Első sorában megmondjuk, hogy az 1-es parancssori argumentum alapján kerül beolvasásra a képállomány. Ettől kezdve dolgozni tudunk A kép méretét a <function>get_width</function> és a <function>get_height</function> szorzatából kapjuk, ezt el is tároljuk egy segédváltozóban. A következő sorban létrehozásra kerül a perceptronunk a <varname>new</varname> operátor segítségével, amely paraméterei balról jobbra haladva: a rétegek száma, 1. réteg neuronjai az inputrétegben, 2. réteg neuronjai az inputrétegben, az eredmény (jelen esetben 1 szám)</para>
    <programlisting language="C"><![CDATA[
      double* image = new double[size];

      for(int i=0; i<png_image.get_width(); ++i)
        for(int j=0; j<png_image.get_height(); ++j)
          image[i*png_image.get_width()+j] = png_image[i][j].red;

      double value = (*p)(image);

      cout<<value;

      delete p;
      delete [] image;
    ]]></programlisting>

      <para>
        Létrehozásra kerül egy <varname>double</varname> típusú
        Az egyik for ciklus végigmegy a kép szélességét alkotó pontokon, a másik pedig a magasságán. Miután végigmentünk a képpontokon, az <varname>image</varname> tárolni fogja a képállomány vörös színkomponensét.
        A <varname>value</varname> értéke a Perceptron <varname>image</varname>-re történő meghívása adja majd. Így a perceptronban tárolásra kerül a vörös színkomponens.
        A <varname>value</varname> változó egy double típusú számot tárol. Ez kiiratásra kerül és töröljük tovább nem használatos elemeket, hiszen a számukra eddig fenntartott memóriaterületet érdemes felszabadítanunk, így a lefoglalt memóriamennyiség újra használható. Programunk ezzel véget is ért.
      </para>
      <para>Fordítjuk és futtatjuk a képen látható módon:</para>

      <mediaobject>
                  <imageobject>
                      <imagedata fileref="images/perceptron.png" scale="60"/>
                  </imageobject>
      </mediaobject>

    </section>        
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
</chapter>                
