#include <stdio.h>
#include <curses.h>
#include <stdlib.h>
#include <unistd.h>

//-lncurses

int
main (void)
{
    int xj = 0, xk = 0, yj = 0, yk = 0;
    int mx = 160, my = 48;

    WINDOW *ablak;
    ablak = initscr ();
    /*noecho();
    cbreak ();
    nodelay (ablak, true);*/

    for (;;)
    {
        xj = (xj - 1) % mx;
        xk = (xk + 1) % mx;

        yj = (yj - 1) % my;
        yk = (yk + 1) % my;

        clear ();

        mvprintw (0, 0,
                  "--------------------------------------------------------------------------------");
        mvprintw (24, 0,
                  "--------------------------------------------------------------------------------");
        mvprintw (abs ((yj + (my - yk)) / 2),
                  abs ((xj + (mx - xk)) / 2), "0");

        refresh ();
        usleep (150000);

    }
    return 0;
}
