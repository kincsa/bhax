#include <iostream>

//egészre mutató mutatót visszaadó függvény

int* returnpointer(int* a)
{
	return a;
}

/*egészet visszaadó 
  és két egészet kapó függvényre mutató mutatót visszaadó, 
egészet kapó függvény*/

int (*pointer (int c)) (int a, int b)
	{
		printf("Siker!\n");
	}	

int main()
{
	int x = 5; //egész

	int *y = &x; //egészre mutató mutató

	int &ref = x; //egész referenciája

	int t1 [10] = {1,2,3,4,5,6,7,8,9,10}; //egészek referenciája

	int (&tref) [10] = t1; //egészek tömbjének referenciája

	int *d[10]; //egészre mutató mutatók tömbje

	int *ertek = returnpointer(t1); //egészre mutató mutatót visszaadó függvény

	//egészre mutató mutatót visszaadó függvényremutató mutató

	int* (*pointer) (int*) = returnpointer;	

	/*függvénymutató egy egészet visszaadó és
	  két egészet kapó függvényre mutató mutatót visszaadó,
	egészet kapó függvényre*/

	int (*(*z) (int)) (int , int h);

	return 0;
}