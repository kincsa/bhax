%{
	#include <string.h>
%}
%%
[a-zA-Z]	{

	switch(yytext[0])
	{
		case 'a' : case 'A' :
			printf("4");
			break;

		case 'b' : case 'B' :
			printf("8");
			break;

		case 'c' : case 'C' :
			printf("<");
			break;

		case 'D' :
			printf("|)");
			break;

		case 'd' :
			printf("o|");
			break;				

		case 'e' : case 'E' :
			printf("3");
			break;

		case 'F' :
			printf("|=");
			break;	

		case 'G' :
			printf("(");
			break;	

		case 'g' :
			printf("9");
			break;

		case 'h' : case 'H' :
			printf("|-|");
			break;

		case 'i' : case 'I' :
			printf("1");
			break;

		case 'j' : case 'J' :
			printf("_|");
			break;

		case 'k' : case 'K' :
			printf("|<");
			break;

		case 'l' : case 'L' :
			printf("|_");
			break;

		case 'm' : case 'M' :
			printf("44");
			break;

		case 'n' : case 'N' :
			printf("И");
			break;		

		case 'o' : case 'O' :
			printf("0");
			break;

		case 'p' : case 'P' :
			printf("|o");
			break;

		case 'q' : case 'Q' :
			printf("O_");
			break;

		case 'r' : case 'R' :
			printf("Я");
			break;

		case 's' : case 'S':
			printf("5");
			break;	
			
		case 't' : case 'T' :
			printf("7");
			break;
		
		case 'u' : case 'U' :
			printf("|_|");
			break;			
		
		case 'v' : case 'V' :
			printf("|/");
			break;

		case 'w' : case 'W' :
			printf("W");
			break;

		case 'x' : case 'X' :
			printf("><");
			break;

		case 'y' : case 'Y' :
			printf("`/");
			break;						

		case 'z' : case 'Z' :
			printf("2");
			break;			

		default :
			printf("%s", yytext);
			break;

	}

}         			
%%
int
main()
{
yylex();
return 0;
}
