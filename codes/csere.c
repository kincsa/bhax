#include<stdio.h>

int main()
{
	int elso= 5;
	int masodik= 10;

	printf("elso erteke:%d, masodik erteke:%d\n",elso, masodik);
	
	elso = elso-masodik; //elso = -5, masodik = 10
	masodik = elso+masodik; // masodik = 15, elso = 5
	elso = masodik-elso; // elso = 10, masodik = 5

	printf("Ertekeik a csere utan: ");

	printf("elso:%d, masodik:%d\n",elso, masodik);
	
	return 0;
}