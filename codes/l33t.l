%{
	#include <string.h>
%}
%%
[a-zA-Z]	{

	switch(yytext[0])
	{
		case 'a' :
			printf("4");
			break;

		case 'b' :
			printf("8");
			break;	
		
		case 'e' :
			printf("3");
			break;

		case 'g' :
			printf("9");
			break;

		case 'l' :
			printf("1");
			break;

		case 's' :
			printf("5");
			break;

		case 'z' :
			printf("2");
			break;

		case 'o' :
			printf("0");
			break;			

		default :
			printf("%s", yytext);
			break;

	}

}         			
%%
int
main()
{
yylex();
return 0;
}
