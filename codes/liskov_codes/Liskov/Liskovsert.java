// ez a T az LSP-ben
class Madar
{
	 public void repul()
	 {
		System.out.print("Tudok repulni!\n");
	 }
}

class Pingvin extends Madar // ezt úgy is lehet/kell olvasni, hogy a pingvin tud repülni
{
}

public class Liskovsert
{
	 public static void fgv(Madar madar)
	 {
		  madar.repul();
	 }

	 public static void main(String[] args)
	{
        Madar pingvin = new Pingvin();
        
        fgv(pingvin);
		 // sérül az LSP, mert a P::fgv röptetné a Pingvint, ami ugye lehetetlen.

	}
}
