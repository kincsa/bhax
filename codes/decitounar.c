#include <stdio.h>

int main()
{
	int number;
	printf("Add meg az atalakitando szamot! \n");
	scanf("%d", &number);
	int counter = 0;
	
	for(int i=number; i>0; --i)
	{
		counter++;
		if(counter%5==0)
			printf("| ");
		else
			printf("|");
	}
	printf("\n");
	return 0;
}