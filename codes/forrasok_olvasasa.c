#include <stdio.h>
#include <signal.h>


void jelkezelo()
{
	printf("%s", "h");
}


int main()
{
	for(;;)
	{
		if(signal(SIGINT, SIG_IGN)!=SIG_IGN)
			signal(SIGINT, jelkezelo);
	}

	return 0;
}